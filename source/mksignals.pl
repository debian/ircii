#! /usr/bin/env perl

# this does not support RT signals yet - big deal

$rcsid = '$eterna: mksignals.pl,v 1.4 2020/11/17 08:11:57 mrg Exp $';
($my_rcsid = $rcsid) =~ s/^\$(.*)\$$/$1/;
$from_rcsid = "/* This file is generated from: $my_rcsid */\n\n";

$part_one = <<'__eop1__';
/*
 * Copyright (c) 2003-2020 Matthew R. Green
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "irc.h"
IRCII_RCSID("@(#)$eterna: mksignals.pl,v 1.4 2020/11/17 08:11:57 mrg Exp $");

#include "irc_std.h"
#include "ircaux.h"
#include "signals.h"

/*
 * If any of these produce compile warnings, please remove the
 * second and later instances from the file.
 */

static const u_char * signals[] = {

__eop1__

sub main {
	print $from_rcsid;
	print $part_one;
	while ($_ = <DATA>) {
		next if /^#/;
		chomp;
		my ($sig, @list) = split;
		my $extra = "";
		my $fullsig = "SIG$sig";
		for $alias (@list) {
			my $fullalias = "SIG$alias";
			$extra .= " && $fullalias != $fullsig";
		}
		print "#if defined($fullsig)$extra\n";
		print "\t\t[$fullsig] = UP(\"$sig\"),\n";
		print "#endif\n\n";
	}
	print $part_two;
}

$part_two = <<'__eop2__';
};

static const int max_signo = ARRAY_SIZE(signals);

const u_char *
get_signal(int signo, int nullok)
{
	if (signo >= 0 && signo <= max_signo) {
		if (signals[signo] == NULL) {
			u_char *newsig = NULL;

			malloc_snprintf(&newsig, "SIG%d", signo);
			signals[signo] = newsig;
		}
		return signals[signo];
	}
	if (nullok)
		return NULL;
	return UP("(NOSIG)");
}
__eop2__

&main();

__DATA__
ABRT
ALRM
ALRM1
BUS
CANCEL
CHLD
CLD
CONT
DANGER
DIL
EMT
FPE
FREEZE
GRANT
HUP
ILL
INFO
INT
IO
IOT ABRT
KAP
KILL
KILLTHR
LOST
LWP
MIGRATE
MSG
PIPE
POLL
PRE
PROF
PWR
QUIT
RETRACT
#RTMAX
#RTMIN
SAK
SEGV
SOUND
STOP
SYS
TERM
THAW
TRAP
TSTP
TTIN
TTOU
URG
USR1
USR2
VIRT
VTALRM
WAITING
WINCH
WINDOW
XCPU
XFSZ
