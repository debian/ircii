/* This file is generated from: eterna: mksignals.pl,v 1.3 2019/01/16 06:26:12 mrg Exp  */

/*
 * Copyright (c) 2003-2020 Matthew R. Green
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "irc.h"
IRCII_RCSID("@(#)$eterna: signals.c,v 1.4 2020/11/17 08:11:57 mrg Exp $");

#include "irc_std.h"
#include "ircaux.h"
#include "signals.h"

/*
 * If any of these produce compile warnings, please remove the
 * second and later instances from the file.
 */

static const u_char * signals[] = {

#if defined(SIGABRT)
		[SIGABRT] = UP("ABRT"),
#endif

#if defined(SIGALRM)
		[SIGALRM] = UP("ALRM"),
#endif

#if defined(SIGALRM1)
		[SIGALRM1] = UP("ALRM1"),
#endif

#if defined(SIGBUS)
		[SIGBUS] = UP("BUS"),
#endif

#if defined(SIGCANCEL)
		[SIGCANCEL] = UP("CANCEL"),
#endif

#if defined(SIGCHLD)
		[SIGCHLD] = UP("CHLD"),
#endif

#if defined(SIGCLD)
		[SIGCLD] = UP("CLD"),
#endif

#if defined(SIGCONT)
		[SIGCONT] = UP("CONT"),
#endif

#if defined(SIGDANGER)
		[SIGDANGER] = UP("DANGER"),
#endif

#if defined(SIGDIL)
		[SIGDIL] = UP("DIL"),
#endif

#if defined(SIGEMT)
		[SIGEMT] = UP("EMT"),
#endif

#if defined(SIGFPE)
		[SIGFPE] = UP("FPE"),
#endif

#if defined(SIGFREEZE)
		[SIGFREEZE] = UP("FREEZE"),
#endif

#if defined(SIGGRANT)
		[SIGGRANT] = UP("GRANT"),
#endif

#if defined(SIGHUP)
		[SIGHUP] = UP("HUP"),
#endif

#if defined(SIGILL)
		[SIGILL] = UP("ILL"),
#endif

#if defined(SIGINFO)
		[SIGINFO] = UP("INFO"),
#endif

#if defined(SIGINT)
		[SIGINT] = UP("INT"),
#endif

#if defined(SIGIO)
		[SIGIO] = UP("IO"),
#endif

#if defined(SIGIOT) && SIGABRT != SIGIOT
		[SIGIOT] = UP("IOT"),
#endif

#if defined(SIGKAP)
		[SIGKAP] = UP("KAP"),
#endif

#if defined(SIGKILL)
		[SIGKILL] = UP("KILL"),
#endif

#if defined(SIGKILLTHR)
		[SIGKILLTHR] = UP("KILLTHR"),
#endif

#if defined(SIGLOST)
		[SIGLOST] = UP("LOST"),
#endif

#if defined(SIGLWP)
		[SIGLWP] = UP("LWP"),
#endif

#if defined(SIGMIGRATE)
		[SIGMIGRATE] = UP("MIGRATE"),
#endif

#if defined(SIGMSG)
		[SIGMSG] = UP("MSG"),
#endif

#if defined(SIGPIPE)
		[SIGPIPE] = UP("PIPE"),
#endif

#if defined(SIGPOLL)
		[SIGPOLL] = UP("POLL"),
#endif

#if defined(SIGPRE)
		[SIGPRE] = UP("PRE"),
#endif

#if defined(SIGPROF)
		[SIGPROF] = UP("PROF"),
#endif

#if defined(SIGPWR)
		[SIGPWR] = UP("PWR"),
#endif

#if defined(SIGQUIT)
		[SIGQUIT] = UP("QUIT"),
#endif

#if defined(SIGRETRACT)
		[SIGRETRACT] = UP("RETRACT"),
#endif

#if defined(SIGSAK)
		[SIGSAK] = UP("SAK"),
#endif

#if defined(SIGSEGV)
		[SIGSEGV] = UP("SEGV"),
#endif

#if defined(SIGSOUND)
		[SIGSOUND] = UP("SOUND"),
#endif

#if defined(SIGSTOP)
		[SIGSTOP] = UP("STOP"),
#endif

#if defined(SIGSYS)
		[SIGSYS] = UP("SYS"),
#endif

#if defined(SIGTERM)
		[SIGTERM] = UP("TERM"),
#endif

#if defined(SIGTHAW)
		[SIGTHAW] = UP("THAW"),
#endif

#if defined(SIGTRAP)
		[SIGTRAP] = UP("TRAP"),
#endif

#if defined(SIGTSTP)
		[SIGTSTP] = UP("TSTP"),
#endif

#if defined(SIGTTIN)
		[SIGTTIN] = UP("TTIN"),
#endif

#if defined(SIGTTOU)
		[SIGTTOU] = UP("TTOU"),
#endif

#if defined(SIGURG)
		[SIGURG] = UP("URG"),
#endif

#if defined(SIGUSR1)
		[SIGUSR1] = UP("USR1"),
#endif

#if defined(SIGUSR2)
		[SIGUSR2] = UP("USR2"),
#endif

#if defined(SIGVIRT)
		[SIGVIRT] = UP("VIRT"),
#endif

#if defined(SIGVTALRM)
		[SIGVTALRM] = UP("VTALRM"),
#endif

#if defined(SIGWAITING)
		[SIGWAITING] = UP("WAITING"),
#endif

#if defined(SIGWINCH)
		[SIGWINCH] = UP("WINCH"),
#endif

#if defined(SIGWINDOW)
		[SIGWINDOW] = UP("WINDOW"),
#endif

#if defined(SIGXCPU)
		[SIGXCPU] = UP("XCPU"),
#endif

#if defined(SIGXFSZ)
		[SIGXFSZ] = UP("XFSZ"),
#endif

};

static const int max_signo = ARRAY_SIZE(signals);

const u_char *
get_signal(int signo, int nullok)
{
	if (signo >= 0 && signo <= max_signo) {
		if (signals[signo] == NULL) {
			u_char *newsig = NULL;

			malloc_snprintf(&newsig, "SIG%d", signo);
			signals[signo] = newsig;
		}
		return signals[signo];
	}
	if (nullok)
		return NULL;
	return UP("(NOSIG)");
}
