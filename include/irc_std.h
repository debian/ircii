/*
 * irc_std.h: header to define things used in all the programs ircii
 * comes with
 *
 * hacked together from various other files by matthew green
 *
 * Copyright (c) 1992-2014 Matthew R. Green.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * @(#)$eterna: irc_std.h,v 1.64 2021/03/14 18:22:31 mrg Exp $
 */

#ifndef irc__irc_std_h
#define irc__irc_std_h

/*
 * these things help with converting to/from char* and u_char*
 */
#define UP(s)			((u_char *)(s))
#define UPP(s)			((u_char **)(s))
#define CP(s)			((char *)(s))
#define CPP(s)			((char **)(s))
#define my_strlen(s)		strlen(CP(s))
#define my_strcmp(d,s)		strcmp(CP(d), CP(s))
#define my_strncmp(d,s,n)	strncmp(CP(d), CP(s), (n))
#define my_strcat(d,s)		strcat(CP(d), CP(s))
#define my_strncat(d,s,n)	strncat(CP(d), CP(s), (n))
#define my_strmcat(d,s,n)	strmcat(UP(d), UP(s), (n))
#define my_strmcpy(d,s,n)	strmcpy(UP(d), UP(s), (n))
#define my_index(s,c)		UP(strchr(CP(s), (c)))
#define my_rindex(s,c)		UP(strrchr(CP(s), (c)))
#define my_atoi(s)		atoi(CP(s))
#define my_atol(s)		atol(CP(s))
#define my_getenv(s)		UP(getenv(CP(s)))

#define ARRAY_SIZE(a)		(sizeof(a) / sizeof(a[0]))

#if !defined(__GNUC__) || __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 5)
#define __attribute__(x)        /* delete __attribute__ if non-gcc or gcc1 */
#endif

#ifndef lint
#define IRCII_RCSID_NAMED(x,n) static const char n[] __attribute__((__used__)) = x
#else
#define IRCII_RCSID_NAMED(x,n)
#endif
#define IRCII_RCSID(x) IRCII_RCSID_NAMED(x,rcsid)

#include <errno.h>

typedef void sigfunc(int);

sigfunc *my_signal(int, sigfunc *, int);
#define MY_SIGNAL(s_n, s_h, m_f) my_signal(s_n, s_h, m_f)

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#if defined(HAVE_MEMORY_H)
# include <memory.h>
#endif /* HAVE_MEMORY_H */

#define IS_ABSOLUTE_PATH(file) ((file)[0] == '/')

#if !defined(HAVE_SOCKLEN_T)
typedef unsigned int socklen_t;
#endif /* HAVE_SOCKLEN_T */

#ifndef HAVE_SNPRINTF
int snprintf(char *str, size_t count, const char *fmt, ...);
#endif /* HAVE_SNPRINTF */

#ifndef HAVE_VSNPRINTF
int vsnprintf(char *str, size_t count, const char *fmt, va_list args);
#endif /* HAVE_VSNPRINTF */

#ifdef INET6
# define SOCKADDR_STORAGE struct sockaddr_storage
# define SS_FAMILY(ss) (ss)->ss_family
#else
# define SOCKADDR_STORAGE struct sockaddr_in
# define SS_FAMILY(ss) (ss)->sin_family
#endif

/* AIX is Special.  This turns on Berkeley sockets which makes wserv work. */
#ifdef _AIX
# define COMPAT_43
#endif

#ifndef HAVE_SSIZE_T
typedef int ssize_t;
#endif

#endif /* irc__irc_std_h */
